package no.ntnu.idatx2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainApplication extends Application {
    private HandOfCards cardHand;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Cardgame");

        BorderPane root = new BorderPane();
        root.setPadding(new Insets(10));

        //Right
        Button dealHandButton = new Button("Deal hand");
        Button checkHandButton = new Button("Check hand");
        VBox buttons = new VBox(dealHandButton, checkHandButton);
        buttons.setAlignment(Pos.BASELINE_CENTER);
        buttons.setSpacing(10);
        buttons.setPadding(new Insets(50, 10, 10, 50));

        //Center
        HBox handOfCardsContainer = new HBox();
        handOfCardsContainer.setStyle("-fx-border-color: black");
        Text handOfCards = new Text();
        handOfCardsContainer.getChildren().add(handOfCards);

        //Bottom
        GridPane checks = new GridPane();
        checks.setHgap(10);
        checks.setPadding(new Insets(10));
        Text sumOfFacesText = new Text();
        Text cardsOfHeartsText = new Text();
        Text flushText = new Text();
        Text queenOfSpadesText = new Text();
        Label sumOfFacesLabel = new Label("Sum of the faces: ");
        sumOfFacesLabel.setLabelFor(sumOfFacesText);
        Label cardsOfHeartsLabel = new Label("Cards of hearts: ");
        cardsOfHeartsLabel.setLabelFor(cardsOfHeartsText);
        Label flushLabel = new Label("Flush: ");
        flushLabel.setLabelFor(flushText);
        Label queenOfSpadesLabel = new Label("Queen of spades: ");
        queenOfSpadesLabel.setLabelFor(queenOfSpadesText);
        checks.add(sumOfFacesLabel, 0, 0);
        checks.add(sumOfFacesText, 1, 0);
        checks.add(cardsOfHeartsLabel, 2, 0);
        checks.add(cardsOfHeartsText, 3, 0);
        checks.add(flushLabel, 0, 1);
        checks.add(flushText, 1, 1);
        checks.add(queenOfSpadesLabel, 2, 1);
        checks.add(queenOfSpadesText, 3, 1);

        //Root setup
        root.setBottom(checks);
        root.setCenter(handOfCardsContainer);
        root.setRight(buttons);

        //Setup button actions
        dealHandButton.setOnAction(actionEvent -> {
            cardHand = new DeckOfCards().dealHand(5);
            handOfCards.setText(cardHand.toString());
        });
        checkHandButton.setOnAction(actionEvent -> {
            if (cardHand != null) {
                sumOfFacesText.setText(String.valueOf(cardHand.getCardFaceValueSum()));
                cardsOfHeartsText.setText(cardHand.getCardsOfHearts());
                flushText.setText(cardHand.hasFiveFlush() ? "Yes" : "No");
                queenOfSpadesText.setText(cardHand.hasQueenOfSpades() ? "Yes" : "No");
            }
        });

        stage.setScene(new Scene(root, 500, 500));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
