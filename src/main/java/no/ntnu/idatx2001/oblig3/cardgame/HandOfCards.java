package no.ntnu.idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a hand of cards.
 */
public class HandOfCards {
    private List<PlayingCard> cards;

    /**
     * Constructs a hand of cards with given list of cards.
     *
     * @param cards list of PlayingCard objects constituting the hand.
     * @throws IllegalArgumentException if cards list is empty.
     */
    public HandOfCards(List<PlayingCard> cards) throws IllegalArgumentException {
        if (cards.size() < 1) {
            throw new IllegalArgumentException("List of cards cannot be empty");
        }
        this.cards = new ArrayList<PlayingCard>(cards);
    }

    /**
     * Returns the sum of all face values of cards.
     *
     * @return sum of face values.
     */
    public int getCardFaceValueSum() {
        return cards
            .stream()
            .map(PlayingCard::getFace)
            .reduce(0, Integer::sum);
    }

    /**
     * Returns a string with all cards of hearts in the hand.
     *
     * @return a String with all cards of hearts. Empty String if none.
     */
    public String getCardsOfHearts() {
        String returnString = cards
            .stream()
            .filter(p -> "H".equals(String.valueOf(p.getSuit())))
            .map(p -> p.getAsString() + " ")
            .reduce("", (a, b) -> a + b)
            .trim();
        return returnString.equals("") ? "No hearts." : returnString;
    }

    /**
     * Tells you if the hand contains a queen of spades.
     *
     * @return true if the contains the card, false if else.
     */
    public boolean hasQueenOfSpades() {
        return cards
                .stream()
                .map(PlayingCard::getAsString)
                .anyMatch("S12"::equals);
    }

    /**
     * Tells you if the hand has a "five flush"
     * or has at least five cards of the same suit.
     *
     * @return true if the hand has "five flush", false if else.
     */
    public boolean hasFiveFlush() {
        boolean spadesFlush = cards
                .stream()
                .map(PlayingCard::getSuit)
                .filter(p -> p == 'S')
                .count() >= 5;
        boolean heartsFlush = cards
                .stream()
                .map(PlayingCard::getSuit)
                .filter(p -> p == 'H')
                .count() >= 5;
        boolean clovesFlush = cards
                .stream()
                .map(PlayingCard::getSuit)
                .filter(p -> p == 'C')
                .count() >= 5;
        boolean diamondsFlush = cards
                .stream()
                .map(PlayingCard::getSuit)
                .filter(p -> p == 'D')
                .count() >= 5;
        return spadesFlush || heartsFlush || clovesFlush || diamondsFlush;
    }

    public List<PlayingCard> getCards() {
        return cards;
    }

    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder();
        for (PlayingCard card : cards) {
            returnString.append(card.getAsString()).append(" ");
        }
        return returnString.toString();
    }
}
