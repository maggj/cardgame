package no.ntnu.idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class representing a deck of 52 playing cards.
 */
public class DeckOfCards {
    private Random random;
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private List<PlayingCard> deck;

    /**
     * Constructs a new deck of cards.
     */
    public DeckOfCards() {
        random = new Random();
        deck = new ArrayList<>();

        for (int i = 1; i <= 13; i++) {
            for (int j = 0; j <= 3; j++) {
                deck.add(new PlayingCard(suit[j], i));
            }
        }
    }

    /**
     * Returns a HandOfCards object with a given number of random cards from the deck.
     *
     * @param n number of cards to deal.
     * @return HandOfCards object with cards.
     * @throws IllegalArgumentException if numbers of cards to deal are less than 1 or more than 52.
     */
    public HandOfCards dealHand(int n) throws IllegalArgumentException {
        if (n < 1 || n > 52) {
            throw new IllegalArgumentException("Hand cannot have less than 1 card or more than 52 cards.");
        }
        ArrayList<PlayingCard> cards = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int indexOfCardInDeck = random.nextInt(deck.size());
            cards.add(deck.get(indexOfCardInDeck));
            deck.remove(indexOfCardInDeck);
        }
        return new HandOfCards(cards);
    }
}
