package no.ntnu.idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {
    @Test
    void testConstructorThrowsExceptionAtIllegalParameters() {
        assertThrows(IllegalArgumentException.class, () -> new PlayingCard('A', 1));
        assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 0));
        assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 14));
    }

    @Test
    void testGetAsStringReturnsCorrectFormat() {
        PlayingCard playingCard = new PlayingCard('S', 12);
        assertEquals("S12", playingCard.getAsString());
    }
}