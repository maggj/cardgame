package no.ntnu.idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
    DeckOfCards deckOfCards;

    @BeforeEach
    void init() {
        deckOfCards = new DeckOfCards();
    }

    @Test
    void testDealHandReturnsCorrectSizedHand() {
        HandOfCards handOfCards = deckOfCards.dealHand(5);
        assertEquals(5, handOfCards.getCards().size());
    }

    @Test
    void testDealHandCannotDealEmptyHand() {
        assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(0));
    }

    @Test
    void testDealHandCannotDealNegativeHand() {
        assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(-1));
    }

    @Test
    void testDealHandCannotDealHandLargerThanDeck() {
        assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(53));
    }

    @Test
    void testDealHandDealsHandWithUniqueCards() {
        HandOfCards handOfCards = deckOfCards.dealHand(5);
        List<PlayingCard> cards = handOfCards.getCards();
        Set<PlayingCard> cardsSet = new HashSet<>(cards);
        assertEquals(cards.size(), cardsSet.size());
    }
}