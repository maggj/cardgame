package no.ntnu.idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {
    private HandOfCards handOfCards;

    @BeforeEach
    void init() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S', 1));
        cards.add(new PlayingCard('H', 2));
        cards.add(new PlayingCard('C', 3));
        cards.add(new PlayingCard('C', 4));
        cards.add(new PlayingCard('D', 5));
        handOfCards = new HandOfCards(cards);
    }

    @Test
    void testGetCardValueSumReturnsCorrect() {
        assertEquals(15, handOfCards.getCardFaceValueSum());
    }

    @Nested
    class testGetCardsOfHands {
        @Test
        void testGetCardsOfHeartsReturnsCorrectStringWhenHeartsArePresent() {
            assertEquals("H2", handOfCards.getCardsOfHearts());
        }

        @Test
        void testGetCardsOfHeartsReturnsNoHeartsStringWhenNoHeartsArePresent() {
            List<PlayingCard> cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 1));
            handOfCards = new HandOfCards(cards);
            assertEquals("No hearts.", handOfCards.getCardsOfHearts());
        }
    }

    @Nested
    class testHasQueenOfSpades {
        @Test
        void testHasQueenOfSpadesReturnsTrueWhenCardIsPresent() {
            List<PlayingCard> cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 12));
            handOfCards = new HandOfCards(cards);
            assertTrue(handOfCards.hasQueenOfSpades());
        }

        @Test
        void testHasQueenOfSpadesReturnsFalseWhenCardIsNotPresent() {
            assertFalse(handOfCards.hasQueenOfSpades());
        }
    }

    @Nested
    class testHasFiveFlush {
        @Test
        void testHasFiveFlushReturnsTrueWhenHandHasFiveFlush() {
            List<PlayingCard> cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 1));
            cards.add(new PlayingCard('S', 2));
            cards.add(new PlayingCard('S', 3));
            cards.add(new PlayingCard('S', 4));
            cards.add(new PlayingCard('S', 5));
            handOfCards = new HandOfCards(cards);
            assertTrue(handOfCards.hasFiveFlush());
        }

        @Test
        void testHasFiveFlushReturnsFalseWhenHandDoesNotHaveFiveFlush() {
            assertFalse(handOfCards.hasFiveFlush());
        }
    }
}